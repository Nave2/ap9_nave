#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

using std::cout;
using std::endl;

template <class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
		cout << arr[i] << endl;
	cout << endl;
}

int main()
{
	int arrSize = 6, sarrSize = 7;
	int arr[] = { 1,5,3,99,4,-5 };
	string sarr[] = { "avi","moshe","dana","zroobaveli", "zebra","ben","david" };
	printArray(arr, arrSize);
	printArray(sarr, sarrSize);
	BSNode<int> intTree(arr[0]);
	for (int i = 1; i < arrSize; i++)
	{
		intTree.insert(arr[i]);
	}
	BSNode<string> strTree(sarr[0]);
	for (int i = 1; i < sarrSize; i++)
	{
		strTree.insert(sarr[i]);
	}
	intTree.printNodes();
	cout << endl;
	strTree.printNodes();
	return 0;
}
