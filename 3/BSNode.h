#include <string>
#include <algorithm>
#include <iostream>
using namespace std;

template <typename T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);

	~BSNode();

	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNode() const; // print current node
	void printNodes() const; //for question 1 part C

public:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};

template <typename T>
BSNode<T>::BSNode<T>(T data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

template <typename T>
BSNode<T>::BSNode<T>(const BSNode<T>& other)
{
	BSNode<T>(other._data);
	if (other._left)
		this->_left = new BSNode<T>(*other._left);
	if (other._right)
		this->_right = new BSNode<T>(*other._right);
}

template <typename T>
BSNode<T>::~BSNode<T>()
{
	if (this->_right)
		delete this->_right;
	if (this->_left)
		delete this->_left;
}

template <typename T>
void BSNode<T>::insert(T value)
{
	if (value > this->getData())
	{
		if (this->_right == nullptr)
			this->_right = new BSNode<T>(value);
		else this->_right->insert(value);
	}
	else if (value < this->getData())
	{
		if (this->_left == nullptr)
			this->_left = new BSNode<T>(value);
		else this->_left->insert(value);
	}
	else // if they are equal
	{
		this->_count++;
	}
}

template <typename T>
BSNode<T>& BSNode<T>::operator=(const BSNode<T>& other)
{
	BSNode<T>(other._data);
	if (other._left)
		this->_left = new BSNode<T>(*other._left);
	if (other._right)
		this->_right = new BSNode<T>(*other._right);
	return *this;
}

template <typename T>
bool BSNode<T>::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr);
}

template <typename T>
T BSNode<T>::getData() const
{
	return this->_data;
}

template <typename T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template <typename T>
BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template <typename T>
bool BSNode<T>::search(T val) const
{
	if (this->_data == val)
		return true;
	if (this->_left && val < this->_data)
		return this->_left->search(val);
	if (this->_right && val > this->_data)
		return this->_right->search(val);
	return false;
}

template <typename T>
int BSNode<T>::getHeight() const
{
	if (this->_left == nullptr && this->_right == nullptr)
		return 0;
	int leftHeight = 0, rightHeight = 0;
	if (this->_left != nullptr)
		leftHeight = 1 + this->_left->getHeight();
	if (this->_right != nullptr)
		rightHeight = 1 + this->_right->getHeight();
	return max(leftHeight, rightHeight);
}

template <typename T>
int BSNode<T>::getDepth(const BSNode<T>& root) const
{
	return getCurrNodeDistFromInputNode(&root);
}

template <typename T>
void BSNode<T>::printNode() const
{
	cout << this->_data << " " << this->_count << endl;
}

template <typename T>
void BSNode<T>::printNodes() const //for question 1 part C
{
	if (this->_left)
		this->_left->printNodes();
	this->printNode();
	if (this->_right)
		this->_right->printNodes();
}

template <typename T>
//assumption: the the nodes (this, node param) are in the same tree (has distance)
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode<T>* node) const //auxiliary function for getDepth
{
	if (node == this)
		return 0;

	if (this->_data > node->_data)
		return 1 + getCurrNodeDistFromInputNode(node->_right);
	else if (this->_data < node->_data) // plain "else" would be ok but just for readability
		return 1 + getCurrNodeDistFromInputNode(node->_left);
	return -1;
}