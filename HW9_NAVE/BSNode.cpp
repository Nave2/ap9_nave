#include "BSNode.h"
#include <algorithm>
#include <iostream>
using namespace std;

BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)
{
	BSNode(other._data);
	if (other._left)
		this->_left = new BSNode(*other._left);
	if (other._right)
		this->_right = new BSNode(*other._right);
}

BSNode::~BSNode()
{
	if (this->_right)
		delete this->_right;
	if (this->_left)
		delete this->_left;
}

void BSNode::insert(std::string value)
{
	if (value > this->getData())
	{
		if (this->_right == nullptr)
			this->_right = new BSNode(value);
		else this->_right->insert(value);
	}
	else if (value < this->getData())
	{
		if (this->_left == nullptr)
			this->_left = new BSNode(value);
		else this->_left->insert(value);
	}
	else // if they are equal
	{
		this->_count++;
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	BSNode(other._data);
	if (other._left)
		this->_left = new BSNode(*other._left);
	if (other._right)
		this->_right = new BSNode(*other._right);
	return *this;
}

bool BSNode::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr);
}

std::string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(std::string val) const
{
	if (this->_data == val)
		return true;
	if (this->_left && val < this->_data)
		return this->_left->search(val);
	if (this->_right && val > this->_data)
		return this->_right->search(val);
	return false;
}

int BSNode::getHeight() const
{
	if (this->_left == nullptr && this->_right == nullptr)
		return 0;
	int leftHeight = 0, rightHeight = 0;
	if (this->_left != nullptr)
		leftHeight = 1 + this->_left->getHeight();
	if (this->_right != nullptr)
		rightHeight = 1 + this->_right->getHeight();
	return max(leftHeight, rightHeight);
}

int BSNode::getDepth(const BSNode& root) const
{
	return getCurrNodeDistFromInputNode(&root);
}

void BSNode::printNode() const
{
	cout << this->_data << " " << this->_count << endl;
}

void BSNode::printNodes() const //for question 1 part C
{
	if (this->_left)
		this->_left->printNodes();
	this->printNode();
	if (this->_right)
		this->_right->printNodes();
}

//assumption: the the nodes (this, node param) are in the same tree (has distance)
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const //auxiliary function for getDepth
{
	if (node == this)
		return 0;

	if (this->_data > node->_data)
		return 1 + getCurrNodeDistFromInputNode(node->_right);
	else if (this->_data < node->_data) // plain "else" would be ok but just for readability
		return 1 + getCurrNodeDistFromInputNode(node->_left);
	return -1;
}