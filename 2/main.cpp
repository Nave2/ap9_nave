#include "simple.h"
#include <iostream>
using namespace std;

template <class T>
int compare(const T a, const T b)
{
	if (a < b) return 1;
	if (a > b) return -1;
	if (a == b) return 0;
}

template <class T>
void bubbleSort(T* arr, size_t size)
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - 1; j++)
		{
			if (compare(arr[j], arr[j + 1]) == -1)
			{
				T temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

template <class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
		cout << arr[i] << endl;
	cout << endl;
}

int main()
{
	float arr[5] = {8,5,4,9,1};
	cout << "float array before sorting:" << endl;
	printArray(arr, 5);
	bubbleSort(arr, 5);
	cout << "float array after sorting:" << endl;
	printArray(arr, 5);

	char charArr[7] = "faedhg";
	cout << "char array before sorting:" << endl;
	printArray(charArr, 6);
	bubbleSort(charArr, 6);
	cout << "char array after sorting:" << endl;
	printArray(charArr, 6);

	Simple simpleArr[5] = { Simple(8), Simple(5), Simple(4), Simple(9), Simple(1) };
	cout << "simple array before sorting:" << endl;
	printArray(simpleArr, 5);
	bubbleSort(simpleArr, 5);
	cout << "simple array after sorting:" << endl;
	printArray(simpleArr, 5);
	return 0;
}