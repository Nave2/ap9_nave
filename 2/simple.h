#include <iostream>
using namespace std;
class Simple
{
private:
	int _value;
public:
	Simple(int value) {
		this->_value = value;
	}
	bool operator<(const Simple& other) const {
		return this->_value < other._value;
	}
	bool operator>(const Simple& other) const {
		return this->_value > other._value;
	}
	bool operator==(const Simple& other) const {
		return this->_value == other._value;
	}
	friend std::ostream& operator<< (std::ostream& out, const Simple& simple)
	{
		out << simple._value;
		return out;
	}
};